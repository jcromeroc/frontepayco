export const user = (state = [], action) => {

    switch (action.type) {
        case 'OBTAIN_TOKEN':
            return action.payload;
        case 'REGISTER_USER':
            return action.payload;
        case 'CLEAR_ALL':
            return [];
        default:
            return state;
    }    
};
