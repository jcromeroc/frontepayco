export const wallet = (state = [], action) => {

    switch (action.type) {
        case 'OBTAIN_WALLET':
            return action.payload;
        case 'INCREASE_WALLET':
            return action.payload;
        case 'CLEAR_ALL':
            return [];
        default:
            return state;
    }    
};
