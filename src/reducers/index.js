import { combineReducers } from 'redux';
import { user } from './userReducer';
import { wallet } from './walletReducer';
import { pay } from './payReducer';

export default combineReducers({
    user,
    wallet,
    pay
});
