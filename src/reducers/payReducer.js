export const pay = (state = [], action) => {

    switch (action.type) {
        case 'SEND_PAY':
            return action.payload;
        case 'CONFIRM_PAY':
            return action.payload;
        case 'CLEAR_ALL':
            return [];
        default:
            return state;
    }    
};
