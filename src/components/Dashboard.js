import React, { useState, useEffect, useCallback } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import { Card, CardContent, Avatar } from '@material-ui/core';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import CloseIcon from '@material-ui/icons/Close';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import PaymentIcon from '@material-ui/icons/Payment';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Alert, AlertTitle } from '@material-ui/lab';

import { Redirect } from 'react-router-dom';
import jwt_decode from 'jwt-decode';

import { connect } from 'react-redux';
import { obtainBalance, rechargeWallet, sendPay, confirmPay, clearAll } from '../actions';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '90vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 350,
  },
  rootCard: {
    height: '100%',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText.anchor,
    paddingTop: theme.spacing(3),
  },
  contentCard: {
    alignItems: 'center',
    display: 'flex'
  },
  titleCard: {
    fontWeight: 700
  },
  avatarCard: {
    backgroundColor: theme.palette.white,
    color: theme.palette.primary.main,
    height: 56,
    width: 56
  },
  iconCard: {
    height: 32,
    width: 32
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}));

export const Dashboard = ({ user, wallet, pay, obtainBalance, rechargeWallet, sendPay, confirmPay, clearAll }) =>{
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [redirect, setRedirect] = useState(null);
    const [userData, setUserData] = useState(null);
    const [tab, setTab] = useState("Dashboard"); 

    const [amountRecharge, setAmountRecharge] = useState("");
    const [amountRechargeError, setAmountRechargeError] = useState(false);
    const [touchAmountRecharge, setTouchAmountRecharge] = useState(false);
    const [alertNotificationRechargeSuccess, setAlertNotificationRechargeSuccess] = useState(false);
    const [alertNotificationRechargeFail, setAlertNotificationRechargeFail] = useState(false);

    const [amountPay, setAmountPay] = useState("");
    const [amountPayError, setAmountPayError] = useState(false);
    const [touchAmountPay, setTouchAmountPay] = useState(false);
    const [alertNotificationPaySuccess, setAlertNotificationPaySuccess] = useState(false);
    const [alertNotificationPayFail, setAlertNotificationPayFail] = useState(false);

    const [tokenPay, setTokenPay] = useState("");

    const [openDialog, setOpenDialog] = useState(false)

    const [loading, setLoading] = useState(false);
    
    const handleDrawerOpen = () => {
        setOpen(true);
    };
    
    const handleDrawerClose = () => {
        setOpen(false);
    };
  
    const handleClose = () => {
      setOpenDialog(false);
    };
    
    const setDashboard = () => {
      setTab("Dashboard");
    };

    const setWallet = () => {
      setTab("Wallet");
    };

    const setPay = () => {
      setTab("Pay");
    };

    const handleChangeAmountRecharge = (event) => {
      setAmountRecharge(event.target.value);
    };

    const handleChangeAmountPay = (event) => {
      setAmountPay(event.target.value);
    };
    
    const handleChangeTokenPay = (event) => {
      setTokenPay(event.target.value);
    };

    const validateFieldsWallet = useCallback(() => {
      if(amountRecharge === "" && touchAmountRecharge) {
        setAmountRechargeError(true);
      } else {
        setAmountRechargeError(false);
      }
    }, [amountRecharge, touchAmountRecharge]);

    useEffect(() => {
      validateFieldsWallet();

      if (wallet.data === "Cartera recargada") {
        setAlertNotificationRechargeSuccess(true);
        setAlertNotificationRechargeFail(false);
        setLoading(false);
      } else if (wallet.data === "No se ha podido recargar la cartera") {
        setAlertNotificationRechargeSuccess(false);
        setAlertNotificationRechargeFail(true);
        setLoading(false);
      } else if (wallet.data === "Balance de cartera") {
        setAlertNotificationRechargeSuccess(false);
        setAlertNotificationRechargeFail(false);
        setLoading(false);
      }
  }, [validateFieldsWallet, wallet]);

  const handleSubmitRecharge = () => {
    setLoading(true);
    
    if(!amountRechargeError && userData) {
        rechargeWallet(user.token, userData.email, userData.document, userData.phone, amountRecharge);
    } else {
        setLoading(false);
    }
  };

  const validateFieldsPay = useCallback(() => {
    if(amountPay === "" && touchAmountPay) {
      setAmountPayError(true);
    } else {
      setAmountPayError(false);
    }
  }, [amountPay, touchAmountPay]);

  useEffect(() => {
    validateFieldsPay();

    if (pay.data === "El código ha sido envíado al correo") {
      setOpenDialog(true);
    } else if (pay.data === "No se ha podido realizar el pago") {
      setAlertNotificationPaySuccess(false);
      setAlertNotificationPayFail(true);
      setLoading(false);
    } else if (pay.data === "El pago se ha realizado correctamente") {
      setAlertNotificationPaySuccess(true);
      setAlertNotificationPayFail(false);
      setLoading(false);
      setOpenDialog(false);
    } else if (pay.data === "No se ha podido confirmar el pago") {
      setAlertNotificationPaySuccess(false);
      setAlertNotificationPayFail(true);
      setLoading(false);
      setOpenDialog(false);
    }
}, [validateFieldsPay, pay]);

const handleSubmitPay = () => {
  setLoading(true);
  
  if(!amountPayError && userData) {
    sendPay(user.token, userData.email, amountPay);
  } else {
    setLoading(false);
  }
};

const payConfirmation = () => {
  if(userData && pay) {
    confirmPay(user.token, userData.email, pay.codigoPago, tokenPay);
  }
};

    const closeSession = () => {
        clearAll();
        setRedirect("/");
    };

    useEffect(() => {
        const storedToken = user.token;
        if (storedToken) {
            setUserData(jwt_decode(storedToken));
        }
    }, [user]);

    useEffect(() => {
        if (userData) {
            obtainBalance(user.token, userData.email, userData.document, userData.phone);
        }
    }, [user, userData, obtainBalance, pay]);

    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

    if (redirect) {
        return <Redirect to={redirect} />
    }

    return (
        <div className={classes.root}>
        <CssBaseline />
        <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
            <Toolbar className={classes.toolbar}>
            <IconButton
                edge="start"
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
            >
                <MenuIcon />
            </IconButton>
            <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                Dashboard
            </Typography>
            <Tooltip title="Cerrar Sesión">
                <IconButton color="inherit" onClick={() => closeSession()}>
                    <CloseIcon />
                </IconButton>
            </Tooltip>
            </Toolbar>
        </AppBar>
        <Drawer
            variant="permanent"
            classes={{
            paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
            }}
            open={open}
        >
            <div className={classes.toolbarIcon}>
            <IconButton onClick={handleDrawerClose}>
                <ChevronLeftIcon />
            </IconButton>
            </div>
            <Divider />
            <List>
                {['Home', 'Recargar', 'Pagar'].map((text, index) => (
                    <ListItem button key={text}>
                    <ListItemIcon>{index === 1 ?
                                    <IconButton onClick={setWallet}> <AccountBalanceWalletIcon /></IconButton> : index === 2 ? 
                                    <IconButton onClick={setPay}><PaymentIcon /></IconButton> : 
                                    <IconButton onClick={setDashboard}><HomeIcon /></IconButton>
                                  }</ListItemIcon>
                    <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
            <Divider />
        </Drawer>
        <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="xl" className={classes.container}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                { tab === "Dashboard" ?
                  <Paper className={fixedHeightPaper}>
                    <Box m={1}>
                      <Typography component="h1" variant="h4" color="inherit" noWrap className={classes.title}>
                          Bienvenido {userData ? userData.name : ""}
                      </Typography>
                    </Box>
                      <Card
                          className={clsx(classes.rootCard)}
                      >
                      <CardContent>
                          <Grid
                          container
                          justify="space-between"
                          >
                          <Grid item>
                              <Typography
                              className={classes.titleCard}
                              color="inherit"
                              gutterBottom
                              variant="body2"
                              >
                              Cartera
                              </Typography>
                              <Typography
                              color="inherit"
                              variant="h3"
                              >
                              ${wallet ? wallet.balance : ""}
                              </Typography>
                          </Grid>
                          <Grid item>
                              <Avatar className={classes.avatarCard}>
                              <AttachMoneyIcon className={classes.iconCard} />
                              </Avatar>
                          </Grid>
                          </Grid>
                      </CardContent>
                      </Card>
                  </Paper>
                : tab === "Wallet" ?
                  <Paper className={fixedHeightPaper}>
                    <Box m={1}>
                      <Typography component="h1" variant="h4" color="inherit" noWrap className={classes.title}>
                          Recargar
                      </Typography>
                    </Box>
                    <Box m={1}>
                      <div className={classes.form}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="amountRecharge"
                            label="Monto"
                            name="amountRecharge"
                            onChange={handleChangeAmountRecharge}
                            error={amountRechargeError}
                            helperText={ amountRechargeError ? "Monto vacío" : ""}
                            onBlur={() => setTouchAmountRecharge(true)}
                            value={amountRecharge}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={() => handleSubmitRecharge()}
                            disabled={!touchAmountRecharge}
                        >
                          {loading ? <CircularProgress color="secondary" /> : "Recargar"}
                        </Button>
                        {alertNotificationRechargeSuccess && !loading ? 
                            <Alert severity="success" action={
                              <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                  setAlertNotificationRechargeSuccess(false);
                                }}
                              >
                                <CloseIcon fontSize="inherit" />
                              </IconButton>
                            }>
                                <AlertTitle>Éxito</AlertTitle>
                                {wallet.data}
                            </Alert> 
                            : alertNotificationRechargeFail && !loading ?
                            <Alert severity="error" action={
                              <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                  setAlertNotificationRechargeFail(false);
                                }}
                              >
                                <CloseIcon fontSize="inherit" />
                              </IconButton>
                            }>
                                <AlertTitle>Error</AlertTitle>
                                {wallet.data}
                            </Alert>
                            :
                            null
                        }
                      </div>
                    </Box>
                  </Paper>
                : tab === "Pay" ?
                  <Paper className={fixedHeightPaper}>
                    <Box m={1}>
                      <Typography component="h1" variant="h4" color="inherit" noWrap className={classes.title}>
                          Pagar
                      </Typography>
                    </Box>
                    <Box m={1}>
                      <div className={classes.form}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="amountPay"
                            label="Monto"
                            name="amountPay"
                            onChange={handleChangeAmountPay}
                            error={amountPayError}
                            helperText={ amountPayError ? "Monto vacío" : ""}
                            onBlur={() => setTouchAmountPay(true)}
                            value={amountPay}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={() => handleSubmitPay()}
                            disabled={!touchAmountPay}
                        >
                          {loading ? <CircularProgress color="secondary" /> : "Pagar"}
                        </Button>
                        <Dialog open={openDialog} onClose={handleClose} aria-labelledby="form-dialog-title">
                          <DialogTitle id="form-dialog-title">Confirmar Pago</DialogTitle>
                          <DialogContent>
                            <DialogContentText>
                              Hemos enviado un código de confirmación a tu correo. Introdúcelo aquí.
                            </DialogContentText>
                            <TextField
                              autoFocus
                              margin="dense"
                              id="tokenPay"
                              label="Código de confirmación"
                              fullWidth
                              onChange={handleChangeTokenPay}
                            />
                          </DialogContent>
                          <DialogActions>
                            <Button onClick={handleClose} color="primary">
                              Cancelar
                            </Button>
                            <Button onClick={payConfirmation} color="primary">
                              Confirmar
                            </Button>
                          </DialogActions>
                        </Dialog>
                        {alertNotificationPaySuccess && !loading ? 
                            <Alert severity="success" action={
                              <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                  setAlertNotificationPaySuccess(false);
                                }}
                              >
                                <CloseIcon fontSize="inherit" />
                              </IconButton>
                            }>
                                <AlertTitle>Éxito</AlertTitle>
                                {pay.data}
                            </Alert> 
                            : alertNotificationPayFail && !loading ?
                            <Alert severity="error" action={
                              <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                  setAlertNotificationPayFail(false);
                                }}
                              >
                                <CloseIcon fontSize="inherit" />
                              </IconButton>
                            }>
                                <AlertTitle>Error</AlertTitle>
                                {pay.data}
                            </Alert>
                            :
                            null
                        }
                      </div>
                    </Box>
                  </Paper>
                : null
                }
                </Grid>
            </Grid>
            </Container>
        </main>
        </div>
    );
}

const mapStateToProps = (state) => {
    return { user: state.user, wallet: state.wallet, pay: state.pay };
}

const mapDispatchToProps = { obtainBalance, rechargeWallet, sendPay, confirmPay, clearAll };

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);