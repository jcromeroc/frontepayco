import React, { useState, useEffect, useCallback } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Alert, AlertTitle } from '@material-ui/lab';

import { Redirect } from 'react-router-dom';

import { connect } from 'react-redux';
import { login } from '../actions';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export const Login = ({ user, login }) => {
    const classes = useStyles();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [emailError, setEmailError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const [touchEmail, setTouchEmail] = useState(false);
    const [touchPassword, setTouchPassword] = useState(false);
    const [redirect, setRedirect] = useState(null);
    const [loading, setLoading] = useState(false);
    const [alertNotification, setAlertNotification] = useState(false);

    const handleChangeEmail = (event) => {
        setEmail(event.target.value);
    };

    const handleChangePassword = (event) => {
        setPassword(event.target.value);
    };

    const validateFields = useCallback(() => {
        const emailPattern = /(.+)@(.+){2,}\.(.+){2,}/;

        if(email === "" && touchEmail) {
            setEmailError(true);
        } else if (!emailPattern.test(email) && touchEmail) {
            setEmailError(true);
        } else {
            setEmailError(false);
        }

        if(password === "" && touchPassword) {
            setPasswordError(true);
        } else {
            setPasswordError(false);
        }
    }, [email, touchEmail, password, touchPassword]);

    useEffect(() => {
        validateFields();

        if (user.code === 200) {
            setRedirect("/dashboard");
        } else if (user.code === 400) {
            setLoading(false);
            setAlertNotification(true);
        }
    }, [validateFields, user]);

    const handleSubmit = () => {
        setLoading(true);

        if(!emailError && !passwordError) {
            login(email, password);
        } else {
            setLoading(false);
        }
    };

    if (redirect) {
        return <Redirect to={redirect} />
    }

    return (
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
            <Avatar className={classes.avatar}>
            <VpnKeyIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
                Loguéate en Epayco
            </Typography>
            <div className={classes.form}>
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email"
                name="email"
                onChange={handleChangeEmail}
                error={emailError}
                helperText={ emailError ? "Email incorrecto" : ""}
                onBlur={() => setTouchEmail(true)}
            />
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                onChange={handleChangePassword}
                error={passwordError}
                helperText={ passwordError ? "Contraseña vacía" : ""}
                onBlur={() => setTouchPassword(true)}
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={() => handleSubmit()}
                disabled={!touchEmail || !touchPassword}
            >
                {loading ? <CircularProgress color="secondary" /> : "Entrar"}
            </Button>
            {alertNotification ? 
                <Alert severity="error">
                    <AlertTitle>Error</AlertTitle>
                    {user.data}
                </Alert> 
                : 
                null
            }
            <Grid container>
                <Grid item>
                <Link href="/register" variant="body2">
                    {"No tienes cuenta? Registrate"}
                </Link>
                </Grid>
            </Grid>
            </div>
        </div>
        </Container>
    );
}

const mapStateToProps = (state) => {
    return { user: state.user };
}

const mapDispatchToProps = { login };

export default connect(mapStateToProps, mapDispatchToProps)(Login);