import React, { useState, useEffect, useCallback } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import CircularProgress from '@material-ui/core/CircularProgress';
import { Alert, AlertTitle } from '@material-ui/lab';

import { Redirect } from 'react-router-dom';

import { connect } from 'react-redux';
import { register } from '../actions';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export const Register = ({ userRegister, register }) => {
    const classes = useStyles();

    const [name, setName] = useState("");
    const [document, setDocument] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");

    const [nameError, setNameError] = useState(false);
    const [documentError, setDocumentError] = useState(false);
    const [phoneError, setPhoneError] = useState(false);
    const [emailError, setEmailError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const [repeatPasswordError, setRepeatPasswordError] = useState(false);

    const [touchName, setTouchName] = useState(false);
    const [touchDocument, setTouchDocument] = useState(false);
    const [touchPhone, setTouchPhone] = useState(false);
    const [touchEmail, setTouchEmail] = useState(false);
    const [touchPassword, setTouchPassword] = useState(false);
    const [touchRepeatPassword, setTouchRepeatPassword] = useState(false);

    const [redirect, setRedirect] = useState(null);
    const [loading, setLoading] = useState(false);
    const [alertNotification, setAlertNotification] = useState(false);

    const handleChangeName = (event) => {
        setName(event.target.value);
    };

    const handleChangeDocument = (event) => {
        setDocument(event.target.value);
    };

    const handleChangePhone = (event) => {
        setPhone(event.target.value);
    };

    const handleChangeEmail = (event) => {
        setEmail(event.target.value);
    };

    const handleChangePassword = (event) => {
        setPassword(event.target.value);
    };

    const handleChangeRepeatPassword = (event) => {
        setRepeatPassword(event.target.value);
    };

    const validateFields = useCallback(() => {
        const emailPattern = /(.+)@(.+){2,}\.(.+){2,}/;
        const phonePattern = /[0][0-9]{3}[0-9]{7}/;

        if(name === "" && touchName) {
            setNameError(true);
        } else {
            setNameError(false);
        }

        if(document === "" && touchDocument) {
            setDocumentError(true);
        } else {
            setDocumentError(false);
        }

        if(phone === "" && touchPhone) {
            setPhoneError(true);
        } else if (!phonePattern.test(phone) && touchPhone) {
            setPhoneError(true);
        } else {
            setPhoneError(false);
        }
        
        if(email === "" && touchEmail) {
            setEmailError(true);
        } else if (!emailPattern.test(email) && touchEmail) {
            setEmailError(true);
        } else {
            setEmailError(false);
        }

        if(password === "" && touchPassword) {
            setPasswordError(true);
        } else {
            setPasswordError(false);
        }

        if(touchPassword && touchRepeatPassword && password !== repeatPassword) {
            setRepeatPasswordError(true);
        } else {
            setRepeatPasswordError(false);
        }
    }, [name, touchName, document, touchDocument, phone, touchPhone, email, touchEmail, password, touchPassword, repeatPassword, touchRepeatPassword]);

    useEffect(() => {
        validateFields();

        if (userRegister.code === 200) {
            setRedirect("/dashboard");
        } else if (userRegister.code === 400) {
            setLoading(false);
            setAlertNotification(true);
        }
    }, [validateFields, userRegister]);

    const handleSubmit = () => {
        setLoading(true);

        if(!nameError && !documentError && !phoneError && !emailError && !passwordError && !repeatPasswordError) {
            register(name, document, phone, email, password);
        } else {
            setLoading(false);
        }
    };

    if (redirect) {
        return <Redirect to={redirect} />
    }

    return (
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
            <Avatar className={classes.avatar}>
            <LockOpenIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
            Registrate en Epayco
            </Typography>
            <div className={classes.form} noValidate>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Nombre"
                    name="name"
                    onChange={handleChangeName}
                    onBlur={() => setTouchName(true)}
                    error={nameError}
                    helperText={ nameError ? "Nombre incorrecto" : ""}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="document"
                    label="Documento"
                    name="document"
                    onChange={handleChangeDocument}
                    onBlur={() => setTouchDocument(true)}
                    error={documentError}
                    helperText={ documentError ? "Documento incorrecto" : ""}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="phone"
                    label="Teléfono"
                    name="phone"
                    onChange={handleChangePhone}
                    onBlur={() => setTouchPhone(true)}
                    error={phoneError}
                    helperText={ phoneError ? "Teléfono incorrecto" : ""}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="email"
                    label="Email"
                    name="email"
                    onChange={handleChangeEmail}
                    onBlur={() => setTouchEmail(true)}
                    error={emailError}
                    helperText={ emailError ? "Email incorrecto" : ""}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    variant="outlined"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    onChange={handleChangePassword}
                    onBlur={() => setTouchPassword(true)}
                    error={passwordError}
                    helperText={ passwordError ? "Contraseña vacía" : ""}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    variant="outlined"
                    required
                    fullWidth
                    name="repeatPassword"
                    label="Repite Password"
                    type="password"
                    id="repeatPassword"
                    onChange={handleChangeRepeatPassword}
                    onBlur={() => setTouchRepeatPassword(true)}
                    error={repeatPasswordError}
                    helperText={ repeatPasswordError ? "La contraseña no coincide" : ""}
                />
                </Grid>
            </Grid>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                disabled={!touchName || !touchDocument || !touchPhone || !touchEmail || !touchPassword || !touchRepeatPassword }
                onClick={() => handleSubmit()}
            >
                {loading ? <CircularProgress color="secondary" /> : "Registrarse"}
            </Button>
            {alertNotification ? 
                <Alert severity="error">
                    <AlertTitle>Error</AlertTitle>
                    {userRegister.data}
                </Alert> 
                : 
                null
            }
            <Grid container justify="flex-end">
                <Grid item>
                <Link href="/" variant="body2">
                    Ya tienes cuenta? Loguéate
                </Link>
                </Grid>
            </Grid>
            </div>
        </div>
        </Container>
    );
}

const mapStateToProps = (state) => {
    return { userRegister: state.user };
}

const mapDispatchToProps = { register };

export default connect(mapStateToProps, mapDispatchToProps)(Register);