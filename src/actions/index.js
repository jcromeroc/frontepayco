import API from '../api';

export const login = (_email, _password) => {
    return async dispatch => {
        const response = await API.post(`/login_check`, {
            _email,
            _password
        });
        dispatch({
            type: 'OBTAIN_TOKEN',
            payload: response.data
        });
    }
};

export const register = (name, document, phone, email, password) => {
    return async dispatch => {
        const response = await API.post(`/users/register`, {
            name,
            email,
            password,
            document,
            phone
        });
        dispatch({
            type: 'REGISTER_USER',
            payload: response.data
        });
    }
};

export const obtainBalance = (token, email, document, phone) => {
    return async dispatch => {
        const response = await API.post(`/wallet/balance`, {
            token,
            email,
            document,
            phone
        });
        dispatch({
            type: 'OBTAIN_WALLET',
            payload: response.data
        });
    }
};

export const rechargeWallet = (token, email, document, phone, amount) => {
    return async dispatch => {
        const response = await API.post(`/wallet/increase`, {
            token,
            email,
            document,
            phone,
            amount
        });
        dispatch({
            type: 'INCREASE_WALLET',
            payload: response.data
        });
    }
};

export const sendPay = (token, email, amount) => {
    return async dispatch => {
        const response = await API.post(`/payment/pay`, {
            token,
            email,
            document,
            amount
        });
        dispatch({
            type: 'SEND_PAY',
            payload: response.data
        });
    }
};

export const confirmPay = (token, email, codigoPago, tokenPay) => {
    return async dispatch => {
        const response = await API.post(`/payment/confirmation`, {
            token,
            email,
            codigoPago,
            tokenPay
        });
        dispatch({
            type: 'CONFIRM_PAY',
            payload: response.data
        });
    }
};

export const clearAll = () => {
    return {
        type: 'CLEAR_ALL',
    };
};

